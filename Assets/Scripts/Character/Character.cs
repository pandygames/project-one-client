﻿using UnityEngine;

namespace PandyGames.ProjectOne.Character
{
    public class Character : MonoBehaviour
    {
        public int PositionX;
        public int PositionY;

        public Renderer Renderer;

        public void Init(int x, int y)
        {
            this.PositionX = x;
            this.PositionY = y;
        }
    }
}
