﻿using System.Collections.Generic;
using PandyGames.ProjectOne.Map;
using PandyGames.ProjectOne.Map.IO;
using PandyGames.ProjectOne.Utilits;
using UnityEngine;

namespace PandyGames.ProjectOne.Character
{
    public class TestHighlight : MonoBehaviour
    {

        public MapBuilder MapBuilder;
        public Dictionary<Tuple<int, int>, bool> HighlightList = new Dictionary<Tuple<int, int>, bool>();
        public int X, Y;

        public int SizeX, SizeY;
        public int MoveSpeed;
        // Use this for initialization
        public TerrainType WalkType = TerrainType.Ground;

        private TilePrototype prototype;
        public int ProrotypeId;
        public int EnemyProrotypeId;
        void Start () {
            //this.prototype = this.MapBuilder.Atlas.Tiles[this.ProrotypeId];
            this.OnPositionChanged(this.X, this.Y);
        }
	
        // Update is called once per frame
        void OnEnable () {
            this.OnPositionChanged(this.X, this.Y);
        }

        private void HighLightEnemy(Tuple<int, int> tuple)
        {
            if ((this.terrainPassability[tuple.X, tuple.Y] & this.WalkType) == this.terrainPassability[tuple.X, tuple.Y])
            {
                if (!this.nextPoints.Contains(tuple))
                {
                    this.nextPoints.Add(tuple);
                    //HighlightList[tuple] = true;
                }

                for (int i = 0; i < this.SizeX; i++)
                    for (int j = 0; j < this.SizeY; j++)
                    {
                        var tuple2 = new Tuple<int, int>(tuple.X + i, tuple.Y + j);
                        if (!this.HighlightList.ContainsKey(tuple2))
                        {
                            //nextPoints.Add(tuple2);
                            this.HighlightList[tuple2] = true;
                        }
                    }
            }
        }

        private void HighLightTiles(Tuple<int, int> tuple )
        {
            if ((this.terrainPassability[tuple.X, tuple.Y] & this.WalkType) == this.terrainPassability[tuple.X, tuple.Y])
            {
                if (!this.nextPoints.Contains(tuple))
                {
                    this.nextPoints.Add(tuple);
                }

                for (int i = 0; i < this.SizeX; i++)
                for (int j = 0; j < this.SizeY; j++)
                {
                    var tuple2 = new Tuple<int, int>(tuple.X + i, tuple.Y + j);
                    if (!this.HighlightList.ContainsKey(tuple2))
                    {
                        this.HighlightList[tuple2] = true;
                    }
                }
            }
        }

        private TerrainType[,] terrainPassability;
        private List<Tuple<int, int>> nextPoints;

        public void OnPositionChanged(int x, int y)
        {
            foreach (var kvp in this.HighlightList)
            {
                this.MapBuilder.RefreshTile(null, kvp.Key.X, kvp.Key.Y, 0);
            }
            this.terrainPassability = this.MapBuilder.Map.Passability[this.SizeX, this.SizeY];
            this.HighlightList = new Dictionary<Tuple<int, int>, bool>();
            //HighlightList[new Tuple<int, int>(x, y)] = true;
            List<Tuple<int, int>> lastPoints = new List<Tuple<int, int>> {new Tuple<int, int>(x, y)};
            for (int step = 0; step < this.MoveSpeed; step++)
            {
                this.nextPoints = new List<Tuple<int, int>>();
                foreach (var tuple in lastPoints)
                {
                    if (tuple.X > 0)
                    {
                        this.HighLightTiles(new Tuple<int, int>(tuple.X - 1, tuple.Y));
                    }
                    if (tuple.X < this.MapBuilder.WorldWidth)
                    {
                        this.HighLightTiles(new Tuple<int, int>(tuple.X + 1, tuple.Y));
                    }
                    if (tuple.Y > 0)
                    {
                        this.HighLightTiles(new Tuple<int, int>(tuple.X, tuple.Y - 1));
                    }
                    if (tuple.Y < this.MapBuilder.WorldHeight)
                    {
                        this.HighLightTiles(new Tuple<int, int>(tuple.X, tuple.Y + 1));
                    }
                }
                lastPoints = this.nextPoints;
            }
            
            foreach (var kvp in this.HighlightList)
            {
                this.MapBuilder.RefreshAllTiles(this.prototype, kvp.Key.X, kvp.Key.Y, MapHelper.PassabilityLayerId);
            }
            if ((this.terrainPassability[x, y] & this.WalkType) == this.terrainPassability[x, y])
            {
                for (int i = 0; i < this.SizeX; i++)
                for (int j = 0; j < this.SizeY; j++)
                {
                    //this.MapBuilder.RefreshAllTiles(this.MapBuilder.Atlas.Tiles[this.EnemyProrotypeId], x + i, y + j, this.MapBuilder.HighLightLevelId);
                }
            }
            this.MapBuilder.SubMapPool.Apply();
        }
    }
}
