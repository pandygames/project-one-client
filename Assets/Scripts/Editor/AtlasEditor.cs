﻿using System.Collections.Generic;
using System.IO;
using PandyGames.ProjectOne.Map;
using SFB;
using UnityEditor;
using UnityEngine;

namespace PandyGames.ProjectOne.Editor
{
    public class AtlasEditor : EditorWindow
    {
        private const string EditorPrefsPath = "ObjectPath";
        public Atlas Atlas;
        private string newTextureListName = "";
        private List<TextureList> textureListToDelete = new List<TextureList>();
        [MenuItem("Window/Atlas Builder Editor %#e")]
        static void Init()
        {
            EditorWindow.GetWindow(typeof(AtlasEditor));
        }

        private string lastOpenedFolder = "Assets/EditorResources/Cut";
        void OnEnable()
        {
            if (EditorPrefs.HasKey(EditorPrefsPath))
            {
                string objectPath = EditorPrefs.GetString(EditorPrefsPath);
                this.Atlas = (AssetDatabase.LoadAssetAtPath(objectPath, typeof(Atlas)) ?? new Atlas()) as Atlas;
            }

        }

        void OnGUI()
        {
            if(this.Atlas!= null)
                GUILayout.Label(AssetDatabase.GetAssetPath(this.Atlas), EditorStyles.boldLabel);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Atlas Editor", EditorStyles.boldLabel);
            if (this.Atlas != null)
            {
                GUILayout.Label(this.Atlas.TileSets.Count.ToString(), EditorStyles.boldLabel);
                GUILayout.Label(this.Atlas.Tiles.Count.ToString(), EditorStyles.boldLabel);
                GUILayout.Label(this.Atlas.SpriteRects.Count.ToString(), EditorStyles.boldLabel);
            }

            if (GUILayout.Button("Open Atlas"))
            {
                this.OpenAtlas();
            }
            if (GUILayout.Button("New Atlas"))
            {
                this.CreateNewAtlas();
            }
            GUILayout.EndHorizontal();

            GUILayout.Space(20);

            if (this.Atlas != null)
            {
                GUILayout.BeginHorizontal();
                this.newTextureListName = GUILayout.TextField(this.newTextureListName, 30);
                if (GUILayout.Button("Add TextureList", GUILayout.ExpandWidth(false)) && this.newTextureListName.Length>0)
                {
                    this.Atlas.TileSets.Add(new TextureList(this.newTextureListName));
                    this.newTextureListName = "";
                }

                GUILayout.EndHorizontal();
                GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));

                foreach (var textureList in this.Atlas.TileSets)
                {
                    this.DrawList(textureList);
                }
                foreach (var textureList in this.textureListToDelete)
                {
                    this.Atlas.TileSets.Remove(textureList);
                }
                this.textureListToDelete.Clear();


                this.Atlas.AtlasMaterial = EditorGUILayout.ObjectField("Material", this.Atlas.AtlasMaterial, typeof(Material), false) as Material;
                this.Atlas.Size= EditorGUILayout.IntField("AtlasSize", this.Atlas.Size);
                //this.AtlasSize = int.Parse(EditorGUILayout.ObjectField("AtlasSize", this.AtlasSize.ToString(), typeof(string), false) as string);
                if (GUILayout.Button("Generate Atlas"))
                {
                    this.GenerateAtlas();
                }
            }
            if (GUI.changed)
            {
                EditorUtility.SetDirty(this.Atlas);
            }
        }


        private void DrawList(TextureList textureList)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Space(10);
            textureList.Name = GUILayout.TextField(textureList.Name, 30, new GUIStyle { fontSize = 15, richText = true });
            //GUILayout.Label(textureList.Name, new GUIStyle { fontSize = 15, richText = true });
            if (GUILayout.Button("Delete TextureList", GUILayout.ExpandWidth(false)))
            {
                this.textureListToDelete.Add(textureList);
            }
            GUILayout.EndHorizontal();
            textureList.TilesetType = (TilesetType)EditorGUILayout.EnumPopup(textureList.TilesetType, GUILayout.Width(100));
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
            {
                if (textureList.CurrentId > 1)
                    textureList.CurrentId--;
            }
            GUILayout.Space(5);
            if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
            {
                if (textureList.CurrentId < textureList.Textures.Count)
                {
                    textureList.CurrentId++;
                }
            }

            GUILayout.Space(60);

            if (GUILayout.Button("Add Item", GUILayout.ExpandWidth(false)))
            {
                string[] absPaths = StandaloneFileBrowser.OpenFilePanel("Select Atlas", this.lastOpenedFolder, "png", true);
                foreach (var absPath in absPaths)
                {
                    if (absPath.Length > 0)
                    {
                        this.lastOpenedFolder = Directory.GetParent(absPath).FullName;
                        Debug.Log(this.lastOpenedFolder);
                        textureList.AddItem((Texture2D)AssetDatabase.LoadAssetAtPath("Assets" + absPath.Remove(0, Application.dataPath.Length), typeof(Texture2D)));
                    }
                }
            }
            if (GUILayout.Button("Delete Item", GUILayout.ExpandWidth(false)))
            {
                textureList.DeleteItem();
            }

            GUILayout.EndHorizontal();

            if (textureList.Textures.Count > 0)
            {
                GUILayout.BeginHorizontal();
                textureList.CurrentId = Mathf.Clamp(EditorGUILayout.IntField("Current Item", textureList.CurrentId, GUILayout.ExpandWidth(false)), 1, textureList.Textures.Count);
                EditorGUILayout.LabelField("of   " + textureList.Textures.Count.ToString() + "  items", "", GUILayout.ExpandWidth(false));
                GUILayout.EndHorizontal();

                textureList.Textures[textureList.CurrentId - 1] = EditorGUILayout.ObjectField("Item Icon", textureList.Textures[textureList.CurrentId - 1], typeof(Texture2D), false) as Texture2D;

                GUILayout.Space(10);

            }
            else
            {
                GUILayout.Label("Atlas is Empty.");
            }
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));
        }

        private void GenerateAtlas()
        {
            var atlasTexture = AtlasHelper.GenerateTextureAtlas(this.Atlas, this.Atlas.Size);
            if (atlasTexture)
            {
                string filePath = EditorUtility.SaveFilePanel("Save Atlas", Directory.GetParent(AssetDatabase.GetAssetPath(this.Atlas)).ToString(), "textureAtlas" + ".png", "png");
                if (filePath.Length > 0)
                {
                    byte[] bytes = atlasTexture.EncodeToPNG();
                    File.WriteAllBytes(filePath, bytes);

                    // make path relative to project
                    filePath = "Assets" + filePath.Remove(0, Application.dataPath.Length);

                    // Make sure LoadAssetAtPath and ImportTexture is going to work
                    AssetDatabase.Refresh();

                    ImportTexture(filePath);
                    Debug.Log(filePath);
                    this.Atlas.MainTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(filePath, typeof(Texture2D));
                }
                else
                {
                    this.Atlas.MainTexture = null;
                }
            }
        }


        void CreateNewAtlas()
        {
            this.Atlas = AtlasHelper.Create();
            if (this.Atlas!=null)
            {
                this.Atlas.TileSets = new List<TextureList>();
                string relPath = AssetDatabase.GetAssetPath(this.Atlas);
                EditorPrefs.SetString(EditorPrefsPath, relPath);
            }
        }

        void OpenAtlas()
        {
            string absPath = EditorUtility.OpenFilePanel("Select Atlas", "Assets", "asset");
            if (absPath.StartsWith(Application.dataPath))
            {
                string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
                Debug.Log(relPath);
                this.Atlas = AssetDatabase.LoadAssetAtPath(relPath, typeof(Atlas)) as Atlas;
            
                if (this.Atlas!=null)
                {
                    if (this.Atlas.TileSets == null)
                        this.Atlas.TileSets = new List<TextureList>();
                    EditorPrefs.SetString(EditorPrefsPath, relPath);
                }
            }
        }

        public static bool ImportTexture(Texture2D texture)
        {
#if UNITY_EDITOR
            if (texture != null)
            {
                return ImportTexture(AssetDatabase.GetAssetPath(texture));
            }
#endif
            return false;
        }

        public static bool ImportTexture(string path)
        {
#if UNITY_EDITOR
            if (path.Length > 0)
            {
                TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
                if (textureImporter)
                {
                    textureImporter.alphaIsTransparency = true; // default
                    textureImporter.anisoLevel = 1; // default
                    textureImporter.borderMipmap = false; // default
                    textureImporter.mipmapEnabled = false; // default
                    textureImporter.compressionQuality = 100;
                    textureImporter.isReadable = true;
                    textureImporter.spritePixelsPerUnit = 100;
                    textureImporter.spriteImportMode = SpriteImportMode.None;
                    textureImporter.wrapMode = TextureWrapMode.Clamp;
                    textureImporter.filterMode = FilterMode.Point;
                    textureImporter.textureFormat = TextureImporterFormat.ARGB32;
                    textureImporter.textureType = TextureImporterType.Default;
                    textureImporter.maxTextureSize = 2048;
                    AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
                }
                return true;
            }
#endif
            return false;
        }

    }
}