﻿using System;
using System.Linq;
using PandyGames.ProjectOne.Map;
using UnityEditor;
using UnityEngine;

namespace PandyGames.ProjectOne.Editor
{
    public static class AtlasHelper
    {
        [MenuItem("Assets/Create/Atlas")]
        public static Atlas Create()
        {
            Atlas asset = ScriptableObject.CreateInstance<Atlas>();

            //var path = EditorUtility.SaveFilePanel("Create Atlas", "Atlases", "Atlas", "asset");
            var path = EditorUtility.SaveFilePanelInProject("Create Atlas", "Atlas", "asset", "Crate new atlas");
            Debug.Log(path);
            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
            return asset;
        }

        //public static int AtlasSize = 2048;
        public static Texture2D GenerateTextureAtlas(Atlas atlas, int atlasSize)
        {
            atlas.SpriteRects.Clear();
            atlas.Tiles.Clear();
            foreach (var atlasTileSet in atlas.TileSets)
            {
                foreach (var texture2D in atlasTileSet.Textures)
                {
                    ImportTexture(texture2D);
                }
            }

            //Need Bag Alghoritm
            Texture2D atlasTexture = new Texture2D(atlasSize, atlasSize);
            Color32[] atlasColors = Enumerable.Repeat<Color32>(new Color32(0, 0, 0, 0), atlasSize * atlasSize).ToArray();
            atlasTexture.SetPixels32(atlasColors);
            int i = 0, j = 0, maxJ = 0;
            int tileCounter = 0;
            int spriteCounter = 0;
            foreach (var atlasTileSet in atlas.TileSets)
            {
                foreach (var texture2D in atlasTileSet.Textures)
                {
                    if (i + texture2D.width >= atlasSize)
                    {
                        j = maxJ;
                        i = 0;
                    }
                    CopyTilesetInAtlas(atlasTexture, texture2D, i, j, texture2D.width, texture2D.height);
                    switch (atlasTileSet.TilesetType)
                    {
                        case TilesetType.Autotile:
                            for (int texJ = 0; texJ < texture2D.height; texJ += 96)
                            for (int texI = 0; texI < texture2D.width; texI += 64)
                            {
                                int[] arr = new int[24];
                                for (int k = 0; k < 24; k++)
                                {
                                    arr[k] = spriteCounter;
                                    atlas.SpriteRects.Add(new Rect(texI + i + 16*(k%4), texJ + j + 16*(k/4), 16, 16));
                                    spriteCounter++;
                                }

                                atlas.Tiles.Add(new TilePrototype
                                {
                                    Type = TilesetType.Autotile,
                                    SpriteIds = arr,
                                    Id = tileCounter,
                                    ShowSpriteRect = new Rect((float)(texI + i) / atlasTexture.width, (float)(texJ + j + 64) / atlasTexture.height, 32f/ atlasTexture.width, 32f/ atlasTexture.height)
                                });
                                tileCounter++;
                            }
                            break;
                        case TilesetType.AutotileWall:
                            for (int texJ = 0; texJ < texture2D.height; texJ += 64)
                            for (int texI = 0; texI < texture2D.width; texI += 64)
                            {
                                int[] arr = new int[16];
                                for (int k = 0; k < 16; k++)
                                {
                                    arr[k] = spriteCounter;
                                    atlas.SpriteRects.Add(new Rect(texI + i + 16 * (k % 4), texJ + j + 16 * (k / 4), 16, 16));
                                    spriteCounter++;
                                }

                                atlas.Tiles.Add(new TilePrototype
                                {
                                    Type = TilesetType.AutotileWall,
                                    SpriteIds = arr,
                                    Id = tileCounter,
                                    ShowSpriteRect = new Rect((float)(texI + i) / atlasTexture.width, (float)(texJ + j) / atlasTexture.height, 64f / atlasTexture.width, 64f / atlasTexture.height)
                                });
                                tileCounter++;
                            }
                            break;
                        case TilesetType.AutoTileFull:
                            for (int texJ = 0; texJ < texture2D.height; texJ += 160)
                            for (int texI = 0; texI < texture2D.width; texI += 64)
                            {
                                int[] arr = new int[16];
                                for (int k = 0; k < 16; k++)
                                {
                                    arr[k] = spriteCounter;
                                    atlas.SpriteRects.Add(new Rect(texI + i + 16 * (k % 4), texJ + j + 16 * (k / 4), 16, 16));
                                    spriteCounter++;
                                }

                                atlas.Tiles.Add(new TilePrototype
                                {
                                    Type = TilesetType.AutotileWall,
                                    SpriteIds = arr,
                                    Id = tileCounter,
                                    ShowSpriteRect = new Rect((float)(texI + i) / atlasTexture.width, (float)(texJ + j) / atlasTexture.height, 64f / atlasTexture.width, 64f / atlasTexture.height)
                                });
                                tileCounter++;

                                arr = new int[24];
                                for (int k = 16; k < 40; k++)
                                {
                                    arr[k - 16] = spriteCounter;
                                    atlas.SpriteRects.Add(new Rect(texI + i + 16 * (k % 4), texJ + j + 16 * (k / 4), 16, 16));
                                    spriteCounter++;
                                }

                                atlas.Tiles.Add(new TilePrototype
                                {
                                    Type = TilesetType.Autotile,
                                    SpriteIds = arr,
                                    Id = tileCounter,
                                    ShowSpriteRect = new Rect((float)(texI + i) / atlasTexture.width, (float)(texJ + j + 64) / atlasTexture.height, 64f / atlasTexture.width, 64f / atlasTexture.height)
                                });
                                tileCounter++;

                            }
                            break;
                        case TilesetType.Simple:
                            for (int k = 0; k < texture2D.width / 32; k++)
                            for (int l = 0; l < texture2D.height/32; l++)
                            {

                                atlas.SpriteRects.Add(new Rect(i + 32 * k, j + 32 * l, 32, 32));
                                atlas.Tiles.Add(new TilePrototype { Type = TilesetType.Simple, SpriteIds = new[] {spriteCounter}, Id = tileCounter });
                                spriteCounter++;
                                tileCounter++;
                            }
                            break;
                    }

                    maxJ = Math.Max(maxJ, j + texture2D.height);
                    i += texture2D.width;

                }
            }
            ImportTexture(atlasTexture);
            atlas.MainTexture = atlasTexture;
            atlasTexture.Apply();
            return atlasTexture;
        }

        public static bool ImportTexture(Texture2D texture)
        {
#if UNITY_EDITOR
            if (texture != null)
            {
                return ImportTexture(AssetDatabase.GetAssetPath(texture), Math.Max(texture.width, texture.height));
            }
#endif
            return false;
        }

        public static bool ImportTexture(string path, int textureSize)
        {
#if UNITY_EDITOR
            if (path.Length > 0)
            {
                TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
                if (textureImporter)
                {
                    textureImporter.alphaIsTransparency = true; // default
                    textureImporter.anisoLevel = 1; // default ?? Pixel perfect need?
                    textureImporter.borderMipmap = false; // default
                    textureImporter.mipmapEnabled = false; // default
                    textureImporter.compressionQuality = CompressionQuality;
                    textureImporter.isReadable = true;
                    textureImporter.spritePixelsPerUnit = PixelToUnits;
                    textureImporter.spriteImportMode = SpriteImportMode.None;
                    textureImporter.wrapMode = TextureWrapMode.Clamp;
                    textureImporter.filterMode = FilterMode.Point;
                    textureImporter.textureFormat = TextureImporterFormat.ARGB32;
                    textureImporter.textureType = TextureImporterType.Default;
                    textureImporter.maxTextureSize = textureSize;
                    AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
                }
                return true;
            }
#endif
            return false;
        }

        private static void CopyTilesetInAtlas(Texture2D _atlasTexture, Texture2D tilesetTex, int dstX, int dstY, int width, int height)
        {
            Color[] atlasColors;
            if (tilesetTex == null)
            {
                atlasColors = Enumerable.Repeat<Color>(new Color(0f, 0f, 0f, 0f), width * height).ToArray();
            }
            else
            {
                atlasColors = tilesetTex.GetPixels();
            }

            _atlasTexture.SetPixels(dstX, dstY, width, height, atlasColors);
        }

        public static float PixelToUnits = 100;
        public static int CompressionQuality = 100;
    }
}