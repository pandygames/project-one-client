﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PandyGames.ProjectOne
{
    [Serializable]
    public class PerfectOverride
    {
        public int ReferenceOrthographicSize;
        public float ReferencePixelsPerUnit;
    }

    public class PixelPerfect : MonoBehaviour
    {
        public int ReferenceOrthographicSize;
        public float ReferencePixelsPerUnit;
        public List<PerfectOverride> Overrides;

        private int lastSize;

        void Start()
        {
            this.UpdateOrthoSize();
        }

        PerfectOverride FindOverride(int size)
        {
            return this.Overrides.FirstOrDefault(x => x.ReferenceOrthographicSize == size);
        }

        void UpdateOrthoSize()
        {
            this.lastSize = Screen.height;

            // first find the reference orthoSize
            float refOrthoSize = (this.ReferenceOrthographicSize / this.ReferencePixelsPerUnit) * 0.5f;

            // then find the current orthoSize
            var overRide = this.FindOverride(this.lastSize);
            float ppu = overRide != null ? overRide.ReferencePixelsPerUnit : this.ReferencePixelsPerUnit;
            float orthoSize = (this.lastSize / ppu) * 0.5f;

            // the multiplier is to make sure the orthoSize is as close to the reference as possible
            float multiplier = Mathf.Max(1, Mathf.Round(orthoSize / refOrthoSize));

            // then we rescale the orthoSize by the multipler
            orthoSize /= multiplier;

            // set it
            this.GetComponent<Camera>().orthographicSize = orthoSize;

            Debug.Log(this.lastSize + " " + orthoSize + " " + multiplier + " " + ppu);
        }

        void Update()
        {
            if (this.lastSize != Screen.height)
                this.UpdateOrthoSize();
        }
    }
}