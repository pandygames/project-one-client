﻿using System.Collections.Generic;
using UnityEngine;

namespace PandyGames.ProjectOne.Map
{

    [System.Serializable]
    public enum TilesetType
    {
        None = -1,
        AutotileAnimated = 0,
        Autotile = 1,
        AutotileDungeon = 2,
        AutotileWall = 3,
        AutoTileFull = 4,
        Object = 5,
        Simple = 6
    }

    [System.Serializable]
    public class Atlas : ScriptableObject
    {
        public int Size;
        public List<TextureList> TileSets = new List<TextureList>();

        public Texture2D MainTexture;

        public Material AtlasMaterial;

        public List<Rect> SpriteRects = new List<Rect>();

        public List<TilePrototype> Tiles = new List<TilePrototype>();

    }
}