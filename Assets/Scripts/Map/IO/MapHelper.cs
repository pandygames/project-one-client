﻿using System.Collections.Generic;

namespace PandyGames.ProjectOne.Map.IO
{
    public static class MapHelper
    {
        public static Dictionary<TerrainType, int> PassabilityToTile = new Dictionary<TerrainType, int> {{ TerrainType.Ground, 0 }};
        public static Dictionary<int, TerrainType> TileToPassability = new Dictionary<int, TerrainType> { { 0, TerrainType.Ground }, { 1, TerrainType.Ground } };
        public static int PassabilityLayerId = 11;
    }
}
