﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;

namespace PandyGames.ProjectOne.Map.IO
{
    public class MapReader
    {

        public static Map Read(string mapName, bool withPassability)
        {
#if UNITY_EDITOR
            string path = "Assets/Resources/Maps/" + mapName + ".map";
#elif UNITY_STANDALONE
            string path = "MyGame_Data/Resources/" + mapName + ".map";
#endif

            Map map = new Map();

            using (FileStream fs = new FileStream(path, FileMode.Open))
            using (BinaryReader reader = new BinaryReader(fs))
            {
                map.Width = reader.ReadInt32();
                map.Height = reader.ReadInt32();
                int layersCount = reader.ReadInt32();

                for (int l = 0; l < layersCount; l++)
                {
                    var key = reader.ReadInt32();
                    var layer = key == MapHelper.PassabilityLayerId ? ReadPassabilityLayer(reader, map) : ReadLayer(reader, map, key);
                    if (key == MapHelper.PassabilityLayerId || withPassability)
                        map.Layers[key] = layer;
                }
            }

            return map;
        }

        public static MapLayer ReadLayer(BinaryReader reader, Map map, int layerId)
        {
            var layer = new MapLayer(map.Width, map.Height);
            var mapPath = reader.ReadString();
            Atlas atlas = AssetDatabase.LoadAssetAtPath<Atlas>(mapPath);
            //map.FillLayer(layerId, atlas);
            for (int i = 0; i < map.Width; i++)
                for (int j = 0; j < map.Width; j++)
                {
                    int tileId = reader.ReadInt32();

                    if (tileId >= 0)
                        layer[i, j] = new TileData() { Prototype = atlas.Tiles[tileId] };
                    else
                        layer[i, j] = new TileData();
                }

            layer.Atlas = atlas;
            return layer;
        }
        public static MapLayer ReadPassabilityLayer(BinaryReader reader, Map map)
        {
            var layer = new MapLayer(map.Width, map.Height);
            var mapPath = reader.ReadString();
            Atlas atlas = AssetDatabase.LoadAssetAtPath<Atlas>(mapPath);
            Debug.Log(atlas);
            //map.FillLayer(MapHelper.PassabilityLayerId, atlas);
            TerrainType[,] terrain = new TerrainType[map.Width, map.Height];
            for (int i = 0; i < map.Width; i++)
                for (int j = 0; j < map.Width; j++)
                {
                    int passability = reader.ReadInt32();
                    terrain[i, j] = (TerrainType)passability;
                    if (passability >= 0)
                        layer[i, j] = new TileData() { Prototype = atlas.Tiles[MapHelper.PassabilityToTile[(TerrainType)passability]] };
                    else
                        layer[i, j] = new TileData();
                }
            map.Passability[1, 1] = terrain;
            layer.Atlas = atlas;
            return layer;
        }
    }
}
