﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace PandyGames.ProjectOne.Map.IO
{
    public class MapWriter
    {
        public static void Write(Map map, string mapName)
        {
#if UNITY_EDITOR
            string path = "Assets/Resources/Maps/" + mapName + ".map";
#elif UNITY_STANDALONE
            string path = "MyGame_Data/Resources/" + mapName + ".map";
#endif

            using (FileStream fs = new FileStream(path, FileMode.Create))
            using (BinaryWriter writer = new BinaryWriter(fs))
            {
                writer.Write(map.Width);
                writer.Write(map.Height);
                writer.Write(map.Layers.Count);
                foreach (var mapLayer in map.Layers)
                {
                    if(mapLayer.Key!=MapHelper.PassabilityLayerId) WriteLayer(writer, mapLayer.Key, mapLayer.Value, map);
                    else WritePassabilityLayer(writer, mapLayer.Key, mapLayer.Value, map);
                }
            }
#if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        public static void WriteLayer(BinaryWriter writer, int layerKey, MapLayer layer, Map map)
        {
            writer.Write(layerKey);
            writer.Write(AssetDatabase.GetAssetPath(layer.Atlas));
            for (int i = 0; i < map.Width; i++)
                for (int j = 0; j < map.Width; j++)
                {
                    writer.Write(layer[i, j].Prototype != null ? layer[i, j].Prototype.Id : -1);
                }
        }
        public static void WritePassabilityLayer(BinaryWriter writer, int layerKey, MapLayer layer, Map map)
        {
            writer.Write(layerKey);
            writer.Write(AssetDatabase.GetAssetPath(layer.Atlas));
            for (int i = 0; i < map.Width; i++)
                for (int j = 0; j < map.Width; j++)
                {
                    writer.Write(layer[i, j].Prototype != null ? (int)MapHelper.TileToPassability[layer[i, j].Prototype.Id] : -1);
                }
        }
    }
}
