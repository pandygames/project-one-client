﻿using System.Collections.Generic;
using PandyGames.ProjectOne.Map.IO;
using UnityEngine;

namespace PandyGames.ProjectOne.Map
{
    public class LevelController : MonoBehaviour
    {
        private readonly List<Character.Character> characters = new List<Character.Character>();
        public MapBuilder MapBuilder;
        private Camera camera;
        public GameObject CharacterPrototype;
        public Character.Character CurrentCharacter;
        // Use this for initialization
        void Awake ()
        {
            this.camera = Camera.main;
            this.MapBuilder.CreateMap(MapReader.Read("test", withPassability: true));
            for (int i = 0; i < 10; i++)
            {
                var character = Instantiate(this.CharacterPrototype, new Vector3(i, -i, -9), Quaternion.identity).GetComponent<Character.Character>();
                character.Init(i, -i);
                this.characters.Add(character);
            }
            this.CurrentCharacter = this.characters[0];
        }

        void Update()
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 100, 1<<8);

            if (hit.collider != null)
            {
                Transform objectHit = hit.transform;
                Debug.Log(hit.transform.name);
                if (this.CurrentCharacter != null)
                    this.CurrentCharacter.GetComponent<Character.Character>().Renderer.material.SetColor("_Color", new Color() { b = 1f, g = 1f, r = 1f, a = 1f });

                objectHit.GetComponent<Character.Character>().Renderer.material.SetColor("_Color", new Color() { b = 0.5f, g = 0.5f, r = 0.5f, a = 1f });
                this.CurrentCharacter = objectHit.GetComponent<Character.Character>(); 
            }
        }

    }
}
