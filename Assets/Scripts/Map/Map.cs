﻿using System.Collections.Generic;

namespace PandyGames.ProjectOne.Map
{
    public class Map
    {
        public int Width;
        public int Height;

        public Dictionary<int, MapLayer> Layers;

        public Passability Passability;


        public Map()
        {
            this.Passability = new Passability();
            this.Layers = new Dictionary<int, MapLayer>();
        }

        public void FillLayer(int layer, Atlas atlas)
        {
            this.Layers[layer] = new MapLayer(this.Width, this.Height);
            this.Layers[layer].Atlas = atlas;
            for (int i = 0; i < this.Width; i++)
                for (int j = 0; j < this.Height; j++)
                {
                    this.Layers[layer][i, j] = new TileData ();
                }
        }

        public void FillLayer(int layer, TilePrototype tilePrototype, Atlas atlas)
        {
            this.Layers[layer] = new MapLayer(this.Width, this.Height);
            this.Layers[layer].Atlas = atlas;
            for (int i = 0; i < this.Width; i++)
                for (int j = 0; j < this.Height; j++)
                {
                    this.Layers[layer][i, j] = new TileData {Prototype = tilePrototype};
                }
        }
    }
}
