﻿using System.Collections.Generic;
using PandyGames.ProjectOne.Map.IO;
using UnityEngine;

namespace PandyGames.ProjectOne.Map
{
    public class MapBuilder : MonoBehaviour
    {
        public const int TileWidth = 32;
        public const int TileHeight = 32;
        public const int TilePartWidth = 16;
        public const int TilePartHeight = 16;
        public const int AutoTilesPerRow = 8;
        public const float PixelToUnits = 100f;

        public const float TileWorldWidth = TileWidth / PixelToUnits;
        public const float TileWorldHeight = TileHeight / PixelToUnits;

        public Map Map;
        public SubMapPool SubMapPool;
    
        public int WorldWidth;
        public int WorldHeight;

        public int SubmapWidth;
        public int SubmapHeight;

        public Transform PassabilityLayer;
        public void InitSubmapData(int subMapWidth, int subMapHeight)
        {
            this.SubmapWidth = subMapWidth;
            this.SubmapHeight = subMapHeight;
        }

        public void CreateMap(Map map)
        {
            this.Map = map;
            this.WorldWidth = map.Width;
            this.WorldHeight = map.Height;

            //this.Map.Passability.Clear();
            if (this.SubMapPool!=null) this.SubMapPool.Clear();
            this.SubMapPool = new SubMapPool(this, this.transform);

            this.RefreshTile(null, 0, 0, MapHelper.PassabilityLayerId);
            this.PassabilityLayer = this.SubMapPool.LayersTransform[MapHelper.PassabilityLayerId];

            foreach (var layer in this.Map.Layers)
            {
                for (int i = 0; i < this.Map.Width; i++)
                for (int j = 0; j < this.Map.Height; j++)
                {
                    this.RefreshTile(null, i, j, layer.Key);
                }
            }
            this.SubMapPool.Apply();
        }

        public bool IsTileSimilar(int autoTileX, int autoTileY, MapLayer layer, int prototypeId)
        {
            if (this.IsValidAutoTilePos(autoTileX, autoTileY))
            {
                TilePrototype t = layer[autoTileX, autoTileY].Prototype;
                return (t != null && t.Id==prototypeId);
            }
            return false;
        }

        private bool IsValidAutoTilePos(int x, int y)
        {
            return (x >= 0 && x < this.WorldWidth && y >= 0 && y < this.WorldHeight);
        }


        private static readonly int[,] AutoTileConverter = 
        {
            {12,  4, 12,  4, 14, 22, 14,  6},                                  //   2  1     4  2
            { 0,  2,  0,  2,  8, 18,  8, 10},                                  //   4  #     #  1           //0  3
            { 3, 11,  3, 11,  1, 19,  1,  9},                                  //                           //1  2
            {15, 13, 15, 13,  7, 23,  7,  5},                                  //   1  #     #  4
        };                                                                     //   2  4     1  2

        private static readonly int[,] AutoTileWallConverter = 
        {
            {12,  4, 14,  6},                                                   //      1     2  
            { 0,  2,  8, 10},                                                   //   2  #     #  1           //0  3
            { 3, 11,  1,  9},                                                   //                           //1  2
            {15, 13,  7,  5},                                                   //   1  #     #  2
        };                                                                      //      2     1  


        public void RefreshTile(TilePrototype prototype, int x, int y, int layer)
        {
            this.SubMapPool.MarkUpdatedTile(x, y, layer);
            MapLayer mapLayer = this.Map.Layers[layer];
            TileData lastTileData = mapLayer[x, y];
            if (prototype == null)
            {
                prototype = lastTileData.Prototype;
            }
            if (prototype == null)
            {
                return;
            }
            if (prototype.Type==TilesetType.None)
            {
                lastTileData.Prototype = prototype;
                return;
            }
            if (prototype.Type == TilesetType.Simple && lastTileData.Prototype.Type==TilesetType.Simple) //swap Tiles
            {
                lastTileData.SpriteIds = prototype.SpriteIds;
                lastTileData.Prototype = prototype;
            }
            else
            {
                int id0 = 0;
                int id1 = 0;
                int id2 = 0; 
                int id3 = 0;
                if (prototype.Type == TilesetType.Autotile)
                {
                    id0 = (this.IsTileSimilar(x, y - 1, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x - 1, y - 1, mapLayer, prototype.Id) ? 2 : 0) +
                          (this.IsTileSimilar(x - 1, y, mapLayer, prototype.Id) ? 4 : 0);
                    id1 = (this.IsTileSimilar(x - 1, y, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x - 1, y + 1, mapLayer, prototype.Id) ? 2 : 0) +
                          (this.IsTileSimilar(x, y + 1, mapLayer, prototype.Id) ? 4 : 0);
                    id2 = (this.IsTileSimilar(x, y + 1, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x + 1, y + 1, mapLayer, prototype.Id) ? 2 : 0) +
                          (this.IsTileSimilar(x + 1, y, mapLayer, prototype.Id) ? 4 : 0);
                    id3 = (this.IsTileSimilar(x + 1, y, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x + 1, y - 1, mapLayer, prototype.Id) ? 2 : 0) +
                          (this.IsTileSimilar(x, y - 1, mapLayer, prototype.Id) ? 4 : 0);
                    id0 = AutoTileConverter[0, id0];
                    id1 = AutoTileConverter[1, id1];
                    id2 = AutoTileConverter[2, id2];
                    id3 = AutoTileConverter[3, id3];
                }
                else if(prototype.Type==TilesetType.AutotileWall)
                {
                    id0 = (this.IsTileSimilar(x, y - 1, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x - 1, y, mapLayer, prototype.Id) ? 2 : 0);
                    id1 = (this.IsTileSimilar(x - 1, y, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x, y + 1, mapLayer, prototype.Id) ? 2 : 0);
                    id2 = (this.IsTileSimilar(x, y + 1, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x + 1, y, mapLayer, prototype.Id) ? 2 : 0);
                    id3 = (this.IsTileSimilar(x + 1, y, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x, y - 1, mapLayer, prototype.Id) ? 2 : 0);
                    id0 = AutoTileWallConverter[0, id0];
                    id1 = AutoTileWallConverter[1, id1];
                    id2 = AutoTileWallConverter[2, id2];
                    id3 = AutoTileWallConverter[3, id3];
                }
                else if (prototype.Type == TilesetType.AutotileDungeon)
                {
                    id0 = (this.IsTileSimilar(x, y - 1, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x - 1, y, mapLayer, prototype.Id) ? 2 : 0);
                    id1 = (this.IsTileSimilar(x - 1, y, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x, y + 1, mapLayer, prototype.Id) ? 2 : 0);
                    id2 = (this.IsTileSimilar(x, y + 1, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x + 1, y, mapLayer, prototype.Id) ? 2 : 0);
                    id3 = (this.IsTileSimilar(x + 1, y, mapLayer, prototype.Id) ? 1 : 0) +
                          (this.IsTileSimilar(x, y - 1, mapLayer, prototype.Id) ? 2 : 0);
                    id0 = AutoTileWallConverter[0, id0];
                    id1 = AutoTileWallConverter[1, id1];
                    id2 = AutoTileWallConverter[2, id2];
                    id3 = AutoTileWallConverter[3, id3];
                }

                int[] spriteArr =
                {
                    prototype.SpriteIds[id0],
                    prototype.SpriteIds[id1],
                    prototype.SpriteIds[id2],
                    prototype.SpriteIds[id3]
                };

                lastTileData.SpriteIds = spriteArr;
                lastTileData.Prototype = prototype;
            }
        }

        public void RefreshAllTiles(TilePrototype selectedPrototype, int changeTileX, int changeTileY, int layer)
        {
            if (this.IsValidAutoTilePos(changeTileX, changeTileY))
            {
                this.RefreshTile(selectedPrototype, changeTileX, changeTileY, layer);
                for (int i = changeTileX - 1; i < changeTileX + 2; i++)
                    for (int j = changeTileY - 1; j < changeTileY + 2; j++)
                    {
                        if (!(i == changeTileX && j == changeTileY) && this.IsValidAutoTilePos(i, j))
                        {
                            this.RefreshTile(null, i, j, layer);
                        }
                    }
            }
        }
    }

    
}
