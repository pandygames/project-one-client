﻿using Karcero.Engine.Models;
using TerrainType = PandyGames.ProjectOne.Map.TerrainType;

namespace Karcero.Engine
{
    public class CarceroMapHelper
    {
        public TerrainType[,] Generate(int mapWidth, int mapHeight)
        {
            var generator = new DungeonGenerator<Cell>();
            var genConf = generator.GenerateA()
                     .DungeonOfSize(mapWidth, mapHeight)
                     .ABitRandom()
                     .SomewhatSparse()
                     .WithMediumChanceToRemoveDeadEnds()
                     .WithLargeSizeRooms()
                     .WithLargeNumberOfRooms().GetConfiguration();
            var map = generator.Generate(genConf);
            TerrainType[,] terrain = new TerrainType[mapWidth, mapHeight];
            for(int i = 0;i<mapWidth;i++)
            for (int j = 0; j < mapHeight; j++)
                switch (map.GetCell(i, j).Terrain)
                {
                    case Models.TerrainType.Door:
                        terrain[i, j] = TerrainType.Ground;
                        break;
                    case Models.TerrainType.Floor:
                        terrain[i, j] = TerrainType.Ground;
                        break;
                    case Models.TerrainType.Rock:
                        terrain[i, j] = TerrainType.Wall;
                        break;
                }
            return terrain;
        }
    }
}