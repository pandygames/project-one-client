﻿using Karcero.Engine.Models;

namespace Karcero.Engine.Contracts
{
    /// <summary>
    /// A mapBuilder processor performs some sort of modification to the mapBuilder. The DungeonGenerator will apply any mapBuilder processors added after the
    /// initial mapBuilder generation process.
    /// </summary>
    /// <typeparam name="T">The actual type of the cell class the mapBuilder is comprised of.</typeparam>
    public interface IMapProcessor<T> where T : class, ICell, new()
    {
        /// <summary>
        /// The method that performs the mapBuilder modification. Will be called by the DungeonGenerator.
        /// </summary>
        /// <param name="map">The mapBuilder to perform the modification on.</param>
        /// <param name="configuration">The configuration for the mapBuilder generation process.</param>
        /// <param name="randomizer">The randomizer to use during the processing.</param>
        void ProcessMap(Map<T> map, DungeonConfiguration configuration, IRandomizer randomizer);
    }
}
