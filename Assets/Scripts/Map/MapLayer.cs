﻿namespace PandyGames.ProjectOne.Map
{
    public class MapLayer
    {
        private readonly TileData[,] tilesData;
        public Atlas Atlas;

        public MapLayer(int x, int y)
        {
            this.tilesData = new TileData[x, y];
        }

        public TileData this[int i, int j]
        {
            get { return this.tilesData[i, j]; }
            set { this.tilesData[i, j] = value; }
        }
    }
}
