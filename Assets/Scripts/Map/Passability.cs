﻿using System;
using System.Collections.Generic;
using PandyGames.ProjectOne.Utilits;
using UnityEngine;

namespace PandyGames.ProjectOne.Map
{
    public class Passability
    {
        private Dictionary<Tuple<int, int>, TerrainType[,]> passabilityArray;

        public Passability()
        {
            this.passabilityArray = new Dictionary<Tuple<int, int>, TerrainType[,]>();
        }

        public void Clear()
        {
            this.passabilityArray = new Dictionary<Tuple<int, int>, TerrainType[,]>();
        }
        public TerrainType[,] this[int key1, int key2]
        {
            get
            {
                var kvp = new Tuple<int, int>(key1, key2);
                if (!this.passabilityArray.ContainsKey(kvp))
                {
                    if(key1==1 && key2==1) throw new Exception("Base array not exist");
                    this.passabilityArray[kvp] = this.GenerateNewPassability(key1, key2);
                }
                return this.passabilityArray[kvp];
            }

            set
            {
                this.passabilityArray[new Tuple<int, int>(key1, key2)] = value; }
        }

        private TerrainType[,] GenerateNewPassability(int x, int y)
        {
            var baseArray = this[1, 1];
            var dim1 = baseArray.GetLength(0);
            var dim2 = baseArray.GetLength(1);
            var returnArray = new TerrainType[dim1-x, dim2-y];
            for (int i = 0; i < dim1 - x; i++)
            for (int j = 0; j < dim2 - y; j++)
            {
                TerrainType resultTerrain = TerrainType.None;
                for (int i1 = 0; i1 < x; i1++)
                for (int j1 = 0; j1 < y; j1++)
                {
                    resultTerrain |= baseArray[i + i1, j + j1];
                }
                returnArray[i, j] = resultTerrain;
            }
            return returnArray;
        }
    }
}
