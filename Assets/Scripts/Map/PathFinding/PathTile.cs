﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PandyGames.ProjectOne.Map.PathFinding
{
    public class PathTile
    {
        public Dictionary<EnemyType, bool> PassabilityInfo = new Dictionary<EnemyType, bool>();
    }

    public enum EnemyType
    {
        Size1X1,
        Size1X1Water,
        Size1X1Flying,
        Size2X2,
        Size2X2Water,
        Size2X2Flying,
        Size3X3,
        Size3X3Water,
        Size3X3Flying,
    }
}