﻿using UnityEngine;
using UnityEngine.Rendering;

namespace PandyGames.ProjectOne.Map
{
    public class SubMap : MonoBehaviour
    {
        private Vector3[] mVertices;
        private Vector2[] mUv;
        private int[] mTriangles;

        public MapBuilder MapBuilder;

        public int TileWidth = 8; // width size of chunk
        public int TileHeight = 4; // height size of chunk
        public int TileLayer;
        public int StartTileX;
        public int StartTileY;

        private MeshFilter mMeshFilter;
        public Atlas Atlas;

        public void Configure(MapBuilder mapBuilder, int layer, int startTileX, int startTileY, int tileWidth, int tileHeight, Atlas atlas)
        {
            this.MapBuilder = mapBuilder;
            this.TileWidth = tileWidth;
            this.TileHeight = tileHeight;
            this.TileLayer = layer;
            this.StartTileX = startTileX;
            this.StartTileY = startTileY;
            this.Atlas = atlas;
            this.transform.gameObject.name = "SubMap " + layer + ":" + startTileX + "_" + startTileY;
            Vector3 vPosition = new Vector3(startTileX*MapBuilder.TileWorldWidth, -startTileY*MapBuilder.TileWorldHeight, -layer);
            this.transform.localPosition = vPosition;

            MeshRenderer meshRenderer = this.GetComponent<MeshRenderer>();

            if (meshRenderer == null)
                meshRenderer = this.transform.gameObject.AddComponent<MeshRenderer>();
            meshRenderer.sharedMaterial = this.Atlas.AtlasMaterial;
            meshRenderer.shadowCastingMode = ShadowCastingMode.Off;
            meshRenderer.receiveShadows = false;

            this.mMeshFilter = this.GetComponent<MeshFilter>();

            if (this.mMeshFilter == null)
            {
                this.mMeshFilter = transform.gameObject.AddComponent<MeshFilter>();
            }

            //this.mMeshFilter = this.GetComponent<MeshFilter>() ?? this.transform.gameObject.AddComponent<MeshFilter>();
        }


        public void ApplyData()
        {

            Mesh mesh;
            if (!Application.isPlaying)
            {
                if (this.mMeshFilter.sharedMesh != null)
                {
                    mesh = this.mMeshFilter.sharedMesh;
                }
                else
                {
                    mesh = new Mesh();
                    this.mMeshFilter.mesh = mesh;
                }
            }
            else
            {
                mesh = this.mMeshFilter.mesh;
            }
            mesh.Clear();

            this.FillData();

            mesh.vertices = this.mVertices;
            mesh.uv = this.mUv;
            mesh.triangles = this.mTriangles;
        }

        public Rect GetSpriteRect(int spriteIdx)
        {
            return this.Atlas.SpriteRects[spriteIdx];
        }

        private void Fill1X32Tile(ref int vertexIdx, ref int triangleIdx, TileData tile, int subTileX, int subTileY)
        {
            float px0 = subTileX * (MapBuilder.TileWorldWidth);
            float py0 = -subTileY * (MapBuilder.TileWorldHeight);
            float px1 = (subTileX + 1) * (MapBuilder.TileWorldWidth);
            float py1 = -(subTileY + 1) * (MapBuilder.TileWorldHeight);

            this.mVertices[vertexIdx + 0] = new Vector3(px0, py0, 0);
            this.mVertices[vertexIdx + 1] = new Vector3(px0, py1, 0);
            this.mVertices[vertexIdx + 2] = new Vector3(px1, py1, 0);
            this.mVertices[vertexIdx + 3] = new Vector3(px1, py0, 0);

            this.mTriangles[triangleIdx + 0] = vertexIdx + 2;
            this.mTriangles[triangleIdx + 1] = vertexIdx + 1;
            this.mTriangles[triangleIdx + 2] = vertexIdx + 0;
            this.mTriangles[triangleIdx + 3] = vertexIdx + 0;
            this.mTriangles[triangleIdx + 4] = vertexIdx + 3;
            this.mTriangles[triangleIdx + 5] = vertexIdx + 2;

            int spriteIdx = tile.GetSpriteId(0);
            Rect sprRect = this.GetSpriteRect(spriteIdx);

            float u0 = (sprRect.xMin) / this.Atlas.MainTexture.width;
            float u1 = (sprRect.xMax) / this.Atlas.MainTexture.width;
            float v0 = (sprRect.yMin) / this.Atlas.MainTexture.height;
            float v1 = (sprRect.yMax) / this.Atlas.MainTexture.height;

            this.mUv[vertexIdx + 0] = new Vector3(u0, v0, 0);
            this.mUv[vertexIdx + 1] = new Vector3(u0, v1, 0);
            this.mUv[vertexIdx + 2] = new Vector3(u1, v1, 0);
            this.mUv[vertexIdx + 3] = new Vector3(u1, v0, 0);

            // increment vectex and triangle idx
            vertexIdx += 4;
            triangleIdx += 6;
        }

        private void Fill4X16Tile(ref int vertexIdx, ref int triangleIdx, TileData tile, int subTileX, int subTileY)
        {
            float px0 = subTileX * (MapBuilder.TileWorldWidth);
            float py0 = -subTileY * (MapBuilder.TileWorldHeight);
            float px1 = (subTileX + 0.5f) * (MapBuilder.TileWorldWidth);
            float py1 = -(subTileY + 0.5f) * (MapBuilder.TileWorldHeight);
            float px2 = (subTileX + 1) * (MapBuilder.TileWorldWidth);
            float py2 = -(subTileY + 1) * (MapBuilder.TileWorldHeight);



            this.mVertices[vertexIdx + 0] = new Vector3(px0, py0, 0);
            this.mVertices[vertexIdx + 1] = new Vector3(px0, py1, 0);
            this.mVertices[vertexIdx + 2] = new Vector3(px1, py1, 0);
            this.mVertices[vertexIdx + 3] = new Vector3(px1, py0, 0);

            this.mTriangles[triangleIdx + 0] = vertexIdx + 2;
            this.mTriangles[triangleIdx + 1] = vertexIdx + 1;
            this.mTriangles[triangleIdx + 2] = vertexIdx + 0;
            this.mTriangles[triangleIdx + 3] = vertexIdx + 0;
            this.mTriangles[triangleIdx + 4] = vertexIdx + 3;
            this.mTriangles[triangleIdx + 5] = vertexIdx + 2;

            int spriteIdx = tile.GetSpriteId(0);
            Rect sprRect = this.GetSpriteRect(spriteIdx);
            float u0 = (sprRect.xMin) / this.Atlas.MainTexture.width;
            float u1 = (sprRect.xMax) / this.Atlas.MainTexture.width;
            float v0 = (sprRect.yMax) / this.Atlas.MainTexture.height;
            float v1 = (sprRect.yMin) / this.Atlas.MainTexture.height;

            this.mUv[vertexIdx + 0] = new Vector3(u0, v0, 0);
            this.mUv[vertexIdx + 1] = new Vector3(u0, v1, 0);
            this.mUv[vertexIdx + 2] = new Vector3(u1, v1, 0);
            this.mUv[vertexIdx + 3] = new Vector3(u1, v0, 0);



            this.mVertices[vertexIdx + 4] = new Vector3(px1, py0, 0);
            this.mVertices[vertexIdx + 5] = new Vector3(px1, py1, 0);
            this.mVertices[vertexIdx + 6] = new Vector3(px2, py1, 0);
            this.mVertices[vertexIdx + 7] = new Vector3(px2, py0, 0);

            this.mTriangles[triangleIdx + 6] = vertexIdx + 6;
            this.mTriangles[triangleIdx + 7] = vertexIdx + 5;
            this.mTriangles[triangleIdx + 8] = vertexIdx + 4;
            this.mTriangles[triangleIdx + 9] = vertexIdx + 4;
            this.mTriangles[triangleIdx + 10] = vertexIdx + 7;
            this.mTriangles[triangleIdx + 11] = vertexIdx + 6;

            spriteIdx = tile.GetSpriteId(3);
            sprRect = this.GetSpriteRect(spriteIdx);
            u0 = (sprRect.xMin) / this.Atlas.MainTexture.width;
            u1 = (sprRect.xMax) / this.Atlas.MainTexture.width;
            v0 = (sprRect.yMax) / this.Atlas.MainTexture.height;
            v1 = (sprRect.yMin) / this.Atlas.MainTexture.height;

            this.mUv[vertexIdx + 4] = new Vector3(u0, v0, 0);
            this.mUv[vertexIdx + 5] = new Vector3(u0, v1, 0);
            this.mUv[vertexIdx + 6] = new Vector3(u1, v1, 0);
            this.mUv[vertexIdx + 7] = new Vector3(u1, v0, 0);



            this.mVertices[vertexIdx + 8] = new Vector3(px0, py1, 0);
            this.mVertices[vertexIdx + 9] = new Vector3(px0, py2, 0);
            this.mVertices[vertexIdx + 10] = new Vector3(px1, py2, 0);
            this.mVertices[vertexIdx + 11] = new Vector3(px1, py1, 0);

            this.mTriangles[triangleIdx + 12] = vertexIdx + 10;
            this.mTriangles[triangleIdx + 13] = vertexIdx + 9;
            this.mTriangles[triangleIdx + 14] = vertexIdx + 8;
            this.mTriangles[triangleIdx + 15] = vertexIdx + 8;
            this.mTriangles[triangleIdx + 16] = vertexIdx + 11;
            this.mTriangles[triangleIdx + 17] = vertexIdx + 10;

            spriteIdx = tile.GetSpriteId(1);
            sprRect = this.GetSpriteRect(spriteIdx);
            u0 = (sprRect.xMin) / this.Atlas.MainTexture.width;
            u1 = (sprRect.xMax) / this.Atlas.MainTexture.width;
            v0 = (sprRect.yMax) / this.Atlas.MainTexture.height;
            v1 = (sprRect.yMin) / this.Atlas.MainTexture.height;

            this.mUv[vertexIdx + 8] = new Vector3(u0, v0, 0);
            this.mUv[vertexIdx + 9] = new Vector3(u0, v1, 0);
            this.mUv[vertexIdx + 10] = new Vector3(u1, v1, 0);
            this.mUv[vertexIdx + 11] = new Vector3(u1, v0, 0);



            this.mVertices[vertexIdx + 12] = new Vector3(px1, py1, 0);
            this.mVertices[vertexIdx + 13] = new Vector3(px1, py2, 0);
            this.mVertices[vertexIdx + 14] = new Vector3(px2, py2, 0);
            this.mVertices[vertexIdx + 15] = new Vector3(px2, py1, 0);

            this.mTriangles[triangleIdx + 18] = vertexIdx + 14;
            this.mTriangles[triangleIdx + 19] = vertexIdx + 13;
            this.mTriangles[triangleIdx + 20] = vertexIdx + 12;
            this.mTriangles[triangleIdx + 21] = vertexIdx + 12;
            this.mTriangles[triangleIdx + 22] = vertexIdx + 15;
            this.mTriangles[triangleIdx + 23] = vertexIdx + 14;

            spriteIdx = tile.GetSpriteId(2);
            sprRect = this.GetSpriteRect(spriteIdx);
            u0 = (sprRect.xMin) / this.Atlas.MainTexture.width;
            u1 = (sprRect.xMax) / this.Atlas.MainTexture.width;
            v0 = (sprRect.yMax) / this.Atlas.MainTexture.height;
            v1 = (sprRect.yMin) / this.Atlas.MainTexture.height;

            this.mUv[vertexIdx + 12] = new Vector3(u0, v0, 0);
            this.mUv[vertexIdx + 13] = new Vector3(u0, v1, 0);
            this.mUv[vertexIdx + 14] = new Vector3(u1, v1, 0);
            this.mUv[vertexIdx + 15] = new Vector3(u1, v0, 0);



            // increment vectex and triangle idx
            vertexIdx += 16;
            triangleIdx += 24;
        }

        void FillData()
        {
            this.mVertices = new Vector3[this.TileWidth * this.TileHeight * 4 * 4]; // 4 subtiles x 4 vertex per tile
            this.mUv = new Vector2[this.mVertices.Length];
            this.mTriangles = new int[this.TileWidth * this.TileHeight * 4 * 2 * 3]; // 4 subtiles x 2 triangles per tile x 3 vertex per triangle

            int vertexIdx = 0;
            int triangleIdx = 0;
            for (int subTileX = 0; subTileX < this.TileWidth; ++subTileX)
            {
                for (int subTileY = 0; subTileY < this.TileHeight; ++subTileY)
                {
                    int tileX = this.StartTileX + subTileX;
                    int tileY = this.StartTileY + subTileY;
                    TileData tile = this.MapBuilder.Map.Layers[this.TileLayer][tileX, tileY];
                    if(tile.Prototype==null) continue;
                    if (tile.Prototype.Type != TilesetType.None)
                    {
                        if (tile.Prototype.Type != TilesetType.Autotile && tile.Prototype.Type != TilesetType.AutotileWall)
                        {
                            this.Fill1X32Tile(ref vertexIdx, ref triangleIdx, tile, subTileX, subTileY);
                        }
                        else
                        {
                            this.Fill4X16Tile(ref vertexIdx, ref triangleIdx, tile, subTileX, subTileY);
                        }    
                    }
                }
            }

            // resize arrays
            System.Array.Resize(ref this.mVertices, vertexIdx);
            System.Array.Resize(ref this.mUv, vertexIdx);
            System.Array.Resize(ref this.mTriangles, triangleIdx);
        }
    }
}
