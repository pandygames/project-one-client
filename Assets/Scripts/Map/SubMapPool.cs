﻿using System.Collections.Generic;
using UnityEngine;

namespace PandyGames.ProjectOne.Map
{
    public struct SubMapPosition
    {
        public int Layer;
        public int X;
        public int Y;

        public SubMapPosition(int x, int y, int layer)
        {
            this.X = x;
            this.Y = y;
            this.Layer = layer;
        }
    }
    public class SubMapPool
    {

        public Dictionary<int, List<Transform>> Objects = new Dictionary<int, List<Transform>>();
        public List<SubMap> SubMapsForUpdate = new List<SubMap>();
        public Dictionary<SubMapPosition, SubMap> SubMaps = new Dictionary<SubMapPosition, SubMap>();
        public MapBuilder MapBuilder;
        public Dictionary<int, Transform> LayersTransform = new Dictionary<int, Transform>();
        public SubMapPool(MapBuilder mapBuilder, Transform parent)
        {
            this.MapBuilder = mapBuilder;
            this.ParentTransform = parent;
        }

        public void MarkUpdatedTile(int tileX, int tileY, int layer)
        {
            SubMap subMap = this.GetSubMap(tileX, tileY, layer);
            if (!this.SubMapsForUpdate.Contains(subMap))
            {
                this.SubMapsForUpdate.Add(subMap);
            }
        }

        public void Clear()
        {
            foreach (var transform in this.LayersTransform)
            {
                Object.Destroy(transform.Value.gameObject);
            }
            foreach (var subMap in this.SubMaps)
            {
                Object.Destroy(subMap.Value.gameObject);
            }
            this.LayersTransform.Clear();
            this.SubMaps.Clear();
        }
        private SubMap GetSubMap(int tileX, int tileY, int layer)
        {
            SubMap subMap;
            int startTileX = tileX - tileX % this.MapBuilder.SubmapWidth;
            int startTileY = tileY - tileY % this.MapBuilder.SubmapHeight;
            if (!this.SubMaps.TryGetValue(new SubMapPosition(startTileX, startTileY, layer), out subMap))
            {
                GameObject subMapObject = new GameObject();
                subMap = subMapObject.AddComponent<SubMap>();
                subMap.Configure(this.MapBuilder, layer, startTileX, startTileY,
                    startTileX + this.MapBuilder.SubmapWidth  > this.MapBuilder.WorldWidth  ? this.MapBuilder.WorldWidth  % this.MapBuilder.SubmapWidth  : this.MapBuilder.SubmapWidth,
                    startTileY + this.MapBuilder.SubmapHeight > this.MapBuilder.WorldHeight ? this.MapBuilder.WorldHeight % this.MapBuilder.SubmapHeight : this.MapBuilder.SubmapHeight,
                    this.MapBuilder.Map.Layers[layer].Atlas);
                this.SubMaps[new SubMapPosition(startTileX, startTileY, layer)] = subMap;
                if (!this.LayersTransform.ContainsKey(layer))
                {
                    var layerObject = new GameObject();
                    layerObject.name = "Layer " + layer;
                    layerObject.transform.parent = this.ParentTransform;
                    this.LayersTransform[layer] = layerObject.transform;
                }
                subMapObject.transform.parent = this.LayersTransform[layer];
                if(!this.Objects.ContainsKey(layer)) this.Objects[layer] = new List<Transform>();
                this.Objects[layer].Add(subMapObject.transform);
                
                //chunkLayer.TileChunks[chunkIdx] = subMap;
                //subMap.Configure(m_autoTileMap, layer, startTileX, startTileY, k_TileChunkWidth, k_TileChunkHeight);
            }

            return subMap;
        }

        public Transform ParentTransform;

        public void Apply()
        {
            foreach (var subMap in this.SubMapsForUpdate)
            {
                //Debug.Log("Update submap");
                subMap.ApplyData();
            }
            this.SubMapsForUpdate.Clear();
        }
    }
}
