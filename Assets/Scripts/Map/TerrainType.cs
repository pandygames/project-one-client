﻿using System;

namespace PandyGames.ProjectOne.Map
{
    [Flags]
    public enum TerrainType
    {
        None      = 0,
        Ground    = 1,
        Water     = 2,
        Wall      = 4,
        Magma     = 8,
        Mountain  = 16,
    }
}
