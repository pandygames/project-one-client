﻿using System.Collections.Generic;
using UnityEngine;

namespace PandyGames.ProjectOne.Map
{
    [System.Serializable]
    public class TextureList
    {
        [SerializeField]
        public List<Texture2D> Textures;

        [SerializeField]
        public int Layer;

        [SerializeField]
        public string Name;

        [SerializeField]
        public TilesetType TilesetType;

        public Texture2D Template;
        public TextureList(string name)
        {
            this.Name = name;
            this.Textures = new List<Texture2D>();
        }

        [SerializeField]
        public int CurrentId = 0;


        public void AddItem(Texture2D texture2D)
        {
            this.Textures.Add(texture2D);
            this.CurrentId = this.Textures.Count;
        }

        public void DeleteItem()
        {
            if(this.Textures.Count>0)
                this.Textures.RemoveAt(this.CurrentId - 1);
        }
    }
}