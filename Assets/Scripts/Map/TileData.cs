﻿using UnityEngine;

namespace PandyGames.ProjectOne.Map
{
    public class TileData
    {
        public int[] SpriteIds;
        public TilePrototype Prototype;
        public int GetSpriteId(int refId)
        {
            return this.SpriteIds[refId];
            //return Random.Range(0, 12);
        }
    }

    [System.Serializable]
    public class TilePrototype
    {
        public TilesetType Type = TilesetType.None;
        public int Id = -1;
        public int[] SpriteIds;
        public Rect ShowSpriteRect;
    }
}
