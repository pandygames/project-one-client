﻿using PandyGames.ProjectOne.Map;
using PandyGames.ProjectOne.Map.IO;
using UnityEngine;

namespace PandyGames.ProjectOne.MapEditor
{
    public class DrawController : MonoBehaviour
    {
        public int TilePanelWidthCount;
        public int TextureSize = 32;

        public MapBuilder MapBuilder;
        public Camera Camera;
        public TilePrototype SelectedPrototype;

        void Update()
        {
            if (Input.GetKey(KeyCode.Alpha1)) this.SelectedLevel = 1;
            if (Input.GetKey(KeyCode.Alpha2)) this.SelectedLevel = 2;
            if (Input.GetKey(KeyCode.Alpha3)) this.SelectedLevel = 3;
            if (Input.GetKey(KeyCode.Alpha4)) this.SelectedLevel = 4;
            if (Input.GetKey(KeyCode.Alpha5)) this.SelectedLevel = 5;
            if (Input.GetKey(KeyCode.Alpha6)) this.SelectedLevel = 6;
            if (Input.GetKey(KeyCode.Alpha7)) this.SelectedLevel = 7;
            if (Input.GetKey(KeyCode.Alpha8)) this.SelectedLevel = 8;
            if (Input.GetKey(KeyCode.Alpha9)) this.SelectedLevel = 9;

            if (Input.GetKey(KeyCode.Alpha0)) this.SelectedLevel = MapHelper.PassabilityLayerId;

            for (int i = 1; i < 10; i++)
            {
                if (i <= this.SelectedLevel)
                    foreach (var variable in this.MapBuilder.SubMapPool.Objects[i])
                        variable.GetComponent<Renderer>().material.SetColor("_Color", new Color() { b = 1f, g = 1f, r = 1f, a = 1f });
                else
                    foreach (var variable in this.MapBuilder.SubMapPool.Objects[i])
                        variable.GetComponent<Renderer>().material.SetColor("_Color", new Color() {b = 1f, g = 1f, r = 1f,a =1- (i-this.selectedLevel)/10f });

            }

            foreach (var variable in this.MapBuilder.SubMapPool.Objects[MapHelper.PassabilityLayerId])
                variable.GetComponent<Renderer>().material.SetColor("_Color", new Color() { b = 1f, g = 1f, r = 1f, a = this.SelectedLevel == MapHelper.PassabilityLayerId?1f:0.5f });
            //this.MapBuilder.PassabilityLayer.gameObject.SetActive(this.SelectedLevel == MapHelper.PassabilityLayerId);
        }
        void OnGUI()
        {
            bool updated = false;
            int i = 0;
            int j = this.TextureSize;
            var currentId = 0;
            if (GUI.RepeatButton(new Rect(0, 0, this.TextureSize, this.TextureSize), ""))
            {
                this.SelectedPrototype = new TilePrototype();
                updated = true;
            }
            foreach (var tilePrototype in this.MapBuilder.Map.Layers[this.SelectedLevel].Atlas.Tiles)
            {
                if (GUI.RepeatButton(new Rect(i, j, this.TextureSize, this.TextureSize), ""))
                {
                    GUI.DrawTextureWithTexCoords(new Rect(i + 4, j + 4, this.TextureSize - 8, this.TextureSize - 8), this.MapBuilder.Map.Layers[this.SelectedLevel].Atlas.MainTexture, tilePrototype.ShowSpriteRect);
                    this.SelectedPrototype = this.MapBuilder.Map.Layers[this.SelectedLevel].Atlas.Tiles[currentId];
                    updated = true;
                }
                else
                {
                    GUI.DrawTextureWithTexCoords(new Rect(i + 2, j + 2, this.TextureSize - 4, this.TextureSize - 4), this.MapBuilder.Map.Layers[this.SelectedLevel].Atlas.MainTexture, tilePrototype.ShowSpriteRect);
                    if (Input.GetKey(KeyCode.LeftShift))
                        GUI.Label(new Rect(i, j, this.TextureSize, this.TextureSize), new GUIContent(tilePrototype.Id.ToString()), new GUIStyle() { fontSize = this.TextureSize / 2, alignment = TextAnchor.MiddleCenter, richText = true });
                }
                i += this.TextureSize;
                if (i >= this.TilePanelWidthCount * this.TextureSize)
                {
                    i = 0;
                    j += this.TextureSize;
                }
                currentId++;
            }

            if (GUI.Button(new Rect(Screen.width - 100, 0, 100, 20), "Generate"))
            {
                this.MapBuilder.CreateMap(this.MapBuilder.Map);
            }
            if (GUI.Button(new Rect(Screen.width - 100, 25, 100, 20), "Save"))
            {
                MapWriter.Write(this.MapBuilder.Map, "test");
            }
            if (GUI.Button(new Rect(Screen.width - 100, 50, 100, 20), "Load"))
            {
                this.MapBuilder.CreateMap(MapReader.Read("test", withPassability: true));
            }

            if (!updated && Input.GetMouseButton(0))
            {
                Vector3 p = this.Camera.ScreenToWorldPoint(Input.mousePosition);

                Vector3 changeTile = new Vector3(p.x / 0.32f, p.y / (-0.32f));

                this.MapBuilder.RefreshAllTiles(this.SelectedPrototype, (int) changeTile.x, (int) changeTile.y, this.SelectedLevel);
                this.MapBuilder.SubMapPool.Apply();
            }
        }

        private int SelectedLevel
        {
            get { return this.selectedLevel; }
            set
            {
                this.selectedLevel = value;
                this.SelectedPrototype = null;
            }
        }

        private int selectedLevel = 1;
    }
}
