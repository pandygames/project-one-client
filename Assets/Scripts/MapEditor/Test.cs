﻿using PandyGames.ProjectOne.Map;
using UnityEngine;

namespace PandyGames.ProjectOne.MapEditor
{
    public class Test: MonoBehaviour
    {
        public MapBuilder MapBuilder;
        public Atlas Atlas;
        public Atlas HighlightAtlas;
        public int XSize, YSize;
        public int BaseTileId;
        void Awake()
        {
            Map.Map map = new Map.Map {Height = this.XSize, Width = this.YSize};

            map.FillLayer(1, this.Atlas.Tiles[this.BaseTileId], this.Atlas);
            for (int i = 2; i < 10; i++)
            {
                map.FillLayer(i, this.Atlas);
            }

            map.FillLayer(11, this.HighlightAtlas);
            map.Passability[1, 1] = new TerrainType[this.XSize, this.YSize];
            this.MapBuilder.InitSubmapData(32, 32);
            this.MapBuilder.CreateMap(map);
        }
    }
}
