﻿namespace PandyGames.ProjectOne.Utilits
{
    public struct Tuple<T1, T2>
    {
        public T1 X;
        public T2 Y;

        public Tuple(T1 x, T2 y)
        {
            this.X = x;
            this.Y = y;
        }

        public override string ToString()
        {
            return this.X + " " + this.Y;
        }
    }
}
